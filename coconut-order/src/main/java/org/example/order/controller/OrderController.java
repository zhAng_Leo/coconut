package org.example.order.controller;

import org.example.order.feign.StockFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @description:
 * @author: lx
 * @date: 2022/4/20 12:45
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    StockFeign stockFeign;

    @RequestMapping("/addOrder")
    public String addOrder() {
        System.out.println("订单新增成功");
        //调用库存扣减
        String apiReqResult = restTemplate.getForObject("http://coconut-stock/stock/subStock", String.class);
        return "订单服务-订单新增成功:" + apiReqResult;
    }

    @RequestMapping("/subOrder")
    public String subOrder() {
        //调用库存扣减
        String apiReqResult = stockFeign.subStock();
        return "订单服务-订单减少生成:" + apiReqResult;
    }

}
