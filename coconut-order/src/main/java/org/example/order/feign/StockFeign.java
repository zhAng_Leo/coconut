package org.example.order.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @description:
 * @author: lx
 * @date: 2022/4/20 15:30
 */
@FeignClient(name = "coconut-stock")
public interface StockFeign {

    @RequestMapping("/stock/subStock")
    String subStock();

}
