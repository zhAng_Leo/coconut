package co.example.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:
 * @author: lx
 * @date: 2022/4/20 12:40
 */
@RestController
public class TestController {

    @GetMapping("/getString")
    public String getString() {
        return "success";
    }

}
